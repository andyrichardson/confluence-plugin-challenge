package com.adaptavist.recruitment.confluence;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.sal.api.ApplicationProperties;

import java.util.Map;

public class MyPluginComponentImpl implements MyPluginComponent, Macro {
    private final ApplicationProperties applicationProperties;

    public MyPluginComponentImpl(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    public String getName() {
        if (null != applicationProperties) {
            return "myComponent:" + applicationProperties.getDisplayName();
        }

        return "myComponent";
    }

    @Override
    public String execute(Map<String, String> map, String s, ConversionContext conversionContext) throws MacroExecutionException {
        return "<h1>HELLO</h1>";
    }

    @Override
    public BodyType getBodyType() {
        return null;
    }

    @Override
    public OutputType getOutputType() {
        return null;
    }
}
